import * as dotenv from 'dotenv'

dotenv.config({ path: __dirname + '/../.env.local' })

export const BaseConfig: Cypress.ConfigOptions = {
    e2e: {
        specPattern: 'cypress/e2e/**/*.ts',
        setupNodeEvents(on) {
            on('task', {
                log(message) {
                    console.log(message)

                    return null
                },
                table(message) {
                    console.table(message)

                    return null
                },
            })
        },
    },
    env: {
        BASIC_AUTH: process.env.CYPRESS_BASIC_AUTH,
    },
    video: false,
    projectId: process.env.CYPRESS_CLOUD_PROJECT_ID,
    chromeWebSecurity: false,
    redirectionLimit: 1000,
}
