import { defineConfig } from 'cypress'

import { BaseConfig } from './cypress.config.base'

import eyesPlugin from '@applitools/eyes-cypress'

const config = {
    ...BaseConfig,
    e2e: {
        ...BaseConfig.e2e,
        baseUrl: 'https://acc.nldoc.nl/',
        excludeSpecPattern: [
            '**/visuele_test.cy.ts',
            '**/redirect-prd.cy.ts',
            '**/vulscript.cy.ts',
        ],
    },
}

export default eyesPlugin(defineConfig(config))
