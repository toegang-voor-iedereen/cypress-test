import { BaseConfig } from './cypress.config.base'

import { defineConfig } from 'cypress'

import eyesPlugin from '@applitools/eyes-cypress'

const config: Cypress.ConfigOptions = {
    ...BaseConfig,
    e2e: {
        ...BaseConfig.e2e,
        baseUrl: 'https://publicatietool.ddev.site/',
        excludeSpecPattern: ['**/visuele_test.cy.ts', '**/vulscript.cy.ts'],
    },
}

export default eyesPlugin(defineConfig(config))
