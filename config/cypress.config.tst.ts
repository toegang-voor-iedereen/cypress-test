import { BaseConfig } from './cypress.config.base'

import { defineConfig } from 'cypress'

import eyesPlugin from '@applitools/eyes-cypress'

const config: Cypress.ConfigOptions = {
    ...BaseConfig,
    e2e: {
        ...BaseConfig.e2e,
        baseUrl: 'https://app.nld-app.ot-t-az1.l12m.nl/',
        excludeSpecPattern: [
            '**/visuele_test.cy.ts',
            '**/redirect-acc.cy.ts',
            '**/redirect-prd.cy.ts',
            '**/vulscript.cy.ts',
        ],
    },
}

export default eyesPlugin(defineConfig(config))
