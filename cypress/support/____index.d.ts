import '@applitools/eyes-cypress'

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Cypress {
        interface Chainable {
            /**
             * Custom command to select DOM element by data-cy attribute.
             * @example cy.dataCy('greeting')
             */
            getIframeBody(): Chainable<unknown>
            login(username: string, password: string): Chainable<JQuery<HTMLElement>>

            deleteDocuments(): Chainable<JQuery<HTMLElement>>

            documentUploadDropzone(documentPath: string): Chainable<JQuery<HTMLElement>>

            documentUploadDropzoneAwaitConversion(
                documentPath: string,
                documentName: string
            ): Chainable<JQuery<HTMLElement>>

            documentUploadSelect(documentPath: string): Chainable<JQuery<HTMLElement>>

            documentUploadSelectAwaitConversion(
                documentPath: string,
                documentName: string
            ): Chainable<JQuery<HTMLElement>>

            editDocument(documentName: string): Chainable<JQuery<HTMLElement>>

            deleteDocument(documentName: string): Chainable<JQuery<HTMLElement>>
        }
    }
}
