Cypress.Commands.add('getIframeBody', () => {
    // get the iframe > document > body
    // and retry until the body element is not empty
    return (
        cy
            .get('iframe')
            .its('0.contentDocument.body')
            .should('not.be.empty')
            // wraps "body" DOM element to allow
            // chaining more Cypress commands, like ".find(...)"
            // https://on.cypress.io/wrap
            .then(cy.wrap)
    )
})

Cypress.Commands.add('login', (username, password) => {
    cy.session(
        [username],
        () => {
            cy.visit('user/login')
            cy.get('[data-drupal-selector="edit-name"]').type(username)
            cy.get('[data-drupal-selector="edit-pass"]').type(password, {
                log: false,
                parseSpecialCharSequences: false,
            })
            cy.get('#edit-submit').click()
            cy.get('span.user__name').should('be.visible')
        },
        {}
    )
})

Cypress.Commands.add('deleteDocuments', () => {
    cy.visit('/documenten')
    cy.url().should('include', '/documenten')

    cy.get('.user__name')
        .first()
        .invoke('attr', 'title')
        .then((username) => {
            cy.step(`Deleting all files on document overview for user: ${username}`)
            cy.get('.view-documents tbody tr')
                .should('have.length.gte', 0)
                .then((el) => {
                    return el
                        ? el.filter((_, element) => element.textContent.includes(username))
                        : []
                })
                .should('have.length.gte', 0)
                .then((row) => {
                    cy.log(`Deleting ${row.length} files on the current page`)
                    row.find('a.delete').each((i, deleteLink) => {
                        cy.visit({
                            url: deleteLink.attributes.getNamedItem('href').value,
                            method: 'GET',
                        }).then(() => {
                            cy.get('#edit-submit').click()
                        })
                    })
                })

            cy.visit('/documenten')
            cy.get('.view-documents tbody tr')
                .should('have.length.gte', 0)
                .then((el) => {
                    if (
                        el.filter((_, element) => element.textContent.includes(username)).length > 0
                    ) {
                        cy.deleteDocuments()
                    }
                })
        })
})

Cypress.Commands.add('documentUploadDropzone', (documentPath) => {
    cy.step('Upload document via drag-and-drop')
    cy.url().should('include', '/documenten')
    cy.get('#pt-dragzone').selectFile(`cypress/fixtures/${documentPath}`, {
        action: 'drag-drop',
    })
    cy.get('#progress-container').should('contain.text', 'succesvol')
})

Cypress.Commands.add('documentUploadDropzoneAwaitConversion', (documentPath, documentName) => {
    cy.documentUploadDropzone(documentPath)

    cy.get('.views-table').scrollIntoView()

    cy.step('Controleer de initiele titel van het document')
    cy.get('.user__name')
        .first()
        .invoke('attr', 'title')
        .then((username) => {
            cy.get('.view-documents tbody tr')
                .filter(
                    (_, element) =>
                        element.textContent.includes(username) &&
                        element.textContent.includes(`${documentName}`) &&
                        !element.textContent.includes('controleren'),
                    { timeout: 60000 }
                )
                .first()
                .find('.views-field-title a')
                .first()
        })
    cy.step('Controleer de titel van het document en de concept status na conversie')
    cy.get('table')
        .find('tbody tr:first-child')
        .within(() => {
            cy.get('td:nth-child(1)').should('contain.text', `${documentName}`)
            cy.get('td:nth-child(4)').should('contain.text', 'Concept')
        })
})

Cypress.Commands.add('documentUploadSelect', (documentPath) => {
    cy.step('Upload document via selectprompt')
    cy.url().should('include', '/documenten')
    cy.contains('.dz-button', 'Selecteer documenten').click()

    cy.get('input[type="file"]').selectFile(`cypress/fixtures/${documentPath}`, {
        force: true,
    })
    cy.get('#progress-container').should('contain.text', 'succesvol')
})

Cypress.Commands.add('documentUploadSelectAwaitConversion', (documentPath, documentName) => {
    cy.documentUploadSelect(documentPath)

    cy.step('Controleer de initiele titel van het document')
    cy.get('.user__name')
        .first()
        .invoke('attr', 'title')
        .then((username) => {
            cy.get('.view-documents tbody tr')
                .filter(
                    (_, element) =>
                        element.textContent.includes(username) &&
                        element.textContent.includes(`${documentName}`) &&
                        !element.textContent.toLowerCase().includes('controleren'),
                    { timeout: 60000 }
                )
                .first()
                .find('.views-field-title a')
                .first()
        })
    cy.step('Controleer de titel van het document na conversie')
    cy.url().should('include', '/documenten')
    cy.get('table')
        .find('tbody tr:first-child')
        .within(() => {
            cy.get('td:nth-child(1)').should('contain.text', `${documentName}`)
        })
    cy.step('Controleer de concept-status van het document')
    cy.url().should('include', '/documenten')
    cy.get('table')
        .find('tbody tr:first-child')
        .within(() => {
            cy.get('td:nth-child(4)').should('contain.text', 'Concept')
        })
})

Cypress.Commands.add('editDocument', (documentName) => {
    cy.step('Klik op document bewerken')
    cy.get('.views-table').within(() => {
        cy.contains('tr', `${documentName}`).then((row) => {
            cy.wrap(row).find('.edit').click({ force: true })
        })
        cy.url().should('include', '/assistant')
    })
})
