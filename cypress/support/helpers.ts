import * as axe from 'axe-core'
export function a11yViolationsLog(violations: axe.Result[]) {
    cy.task(
        'log',
        `${violations.length} accessibility violation${violations.length === 1 ? '' : 's'} ${
            violations.length === 1 ? 'was' : 'were'
        } detected`
    )
    // pluck specific keys to keep the table readable
    const violationData = violations.map(({ id, impact, description, nodes }) => ({
        id,
        impact,
        description,
        nodes: nodes.length,
    }))

    cy.task('table', violationData)
}

export function visitPath({
    path,
    failOnStatusCode = true,
    timeout = 4000,
}: {
    path: string
    failOnStatusCode?: boolean
    timeout?: number
}) {
    const baseUrl = Cypress.config('baseUrl')
    expect(baseUrl).to.be.a('string', 'baseUrl should be set')

    const url = new URL(baseUrl)
    url.pathname = path

    const basicAuth = Cypress.env('BASIC_AUTH')
    if (basicAuth) {
        expect(basicAuth).to.be.a('string', 'Basic auth should be a string')
        expect(basicAuth).contains(':', 'Basic auth string not formatted correctly')

        const basicAuthParts = basicAuth.split(':')
        url.username = basicAuthParts[0]
        url.password = basicAuthParts[1]
    }
    return cy.request({ url: url.href, failOnStatusCode, timeout })
}
