/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Visuele tests op het burgerthema', () => {
    beforeEach(() => {
        cy.eyesOpen({
            // The name of the app under test
            appName: 'NLdoc burger thema',
            // The name of the test case
            testName: Cypress.currentTest.title,
        })
    })

    afterEach(() => {
        cy.eyesClose()
    })

    it.skip('Het burgerthema voldoet aan het ontwerp', () => {
        //ToDo
    })

    it.skip('De huisstijl van X is succesvol toegepast op het burgerthema', () => {
        //ToDo
    })
})
