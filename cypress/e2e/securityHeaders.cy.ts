/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

function expectUrlHasAllRequiredHeaders(url: string): void {
    //TODO: lokaal deze testen uitzetten.
    cy.request({ url, failOnStatusCode: false, followRedirect: false }).then((response) => {
        expect(response.headers).to.include({
            'referrer-policy': 'strict-origin-when-cross-origin',
            'content-security-policy':
                "default-src 'self'; img-src https://* data:; object-src 'none'; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' https://statistiek.rijksoverheid.nl;",
            'x-content-type-options': 'nosniff',
            'strict-transport-security': 'max-age=31536000; includeSubDomains; preload',
            'x-frame-options': 'SAMEORIGIN',
            'cache-control': 'must-revalidate, no-cache, private',
            'cross-origin-resource-policy': 'same-origin',
        })
    })
}

describe('Alle paginas bevatten de juiste security headers', () => {
    it('De root bevat de juiste headers', (): void => expectUrlHasAllRequiredHeaders('./'))
    it('Homepagina bevat de juiste headers', (): void => expectUrlHasAllRequiredHeaders('./home'))
    it('Loginpagina bevat de juiste headers', (): void =>
        expectUrlHasAllRequiredHeaders('/user/login'))
    it('Documentenoverzicht bevat de juiste headers', (): void =>
        expectUrlHasAllRequiredHeaders('/documenten'))
})
