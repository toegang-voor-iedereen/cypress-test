/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Redirection test van HTTPS', () => {
    it('URL redirect van HTTP naar HTTPS', () => {
        cy.request({
            url: 'http://acc.nldoc.nl',
            followRedirect: false,
            timeout: 4000,
        }).then((response) => {
            expect(response.status).to.eq(302)

            const redirectLocation = response.headers['location']
            cy.log(`${redirectLocation}`)
            expect(redirectLocation.includes('https')).to.be.true
            expect(redirectLocation.includes('www')).to.be.false
        })
    })
})
