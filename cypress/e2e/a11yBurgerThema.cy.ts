import { Options } from 'cypress-axe'
import { a11yViolationsLog, visitPath } from 'cypress/support/helpers'
import * as X2JS from 'x2js'

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Accessibility van de geconverteerde paginas in het burgerthema na publicatie', () => {
    const A11Y_OPTIONS: Options = {
        runOnly: { type: 'tag', values: ['wcag21aa'] },
    }

    it('Paginas burgerthema zijn toegankelijk', () => {
        visitPath({ path: 'sitemap.xml' })
            .its('body')
            .then((body) => {
                const x2js = new X2JS()
                const {
                    urlset: { url },
                }: {
                    urlset: { url: { loc: string }[] | { loc: string } }
                } = x2js.xml2js(body)

                const urls = Array.isArray(url) ? url : [url]

                urls.forEach((u) => {
                    cy.visit(u.loc)
                    cy.injectAxe({
                        axeCorePath: './node_modules/axe-core/axe.min.js',
                    })

                    cy.checkA11y(
                        {
                            exclude: [['#toolbar-administration'], ['#primary-tabs-title']],
                        },
                        A11Y_OPTIONS,
                        a11yViolationsLog
                    )
                })
            })
    })
})
