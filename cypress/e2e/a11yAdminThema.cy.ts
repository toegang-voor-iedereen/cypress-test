import { Options } from 'cypress-axe'
import { a11yViolationsLog } from '../support/helpers'

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Accessibility van de paginas in het admin thema', () => {
    const A11Y_OPTIONS: Options = {
        runOnly: { type: 'tag', values: ['wcag21aa'] },
    }

    it('De landingpagina is toegankelijk', () => {
        cy.visit('/')
        cy.injectAxe({
            axeCorePath: './node_modules/axe-core/axe.min.js',
        })

        cy.checkA11y(null, A11Y_OPTIONS, a11yViolationsLog)
    })

    it('De loginpagina is toegankelijk', () => {
        cy.visit('/user/login')
        cy.injectAxe({
            axeCorePath: './node_modules/axe-core/axe.min.js',
        })

        cy.checkA11y(null, A11Y_OPTIONS, a11yViolationsLog)
    })

    it('De documentoverzichtspagina is toegankelijk', () => {
        cy.login(Cypress.env('username3'), Cypress.env('password3'))
        cy.visit('/documenten')
        cy.injectAxe({
            axeCorePath: './node_modules/axe-core/axe.min.js',
        })

        cy.checkA11y(null, A11Y_OPTIONS, a11yViolationsLog)
    })

    it('De assistantpagina is toegankelijk', () => {
        cy.login(Cypress.env('username3'), Cypress.env('password3'))
        cy.visit('/documenten')
        cy.documentUploadSelectAwaitConversion('docx/9.docx', 'Document 9')

        cy.editDocument('Document 9')

        cy.injectAxe({
            axeCorePath: './node_modules/axe-core/axe.min.js',
        })

        cy.checkA11y(null, A11Y_OPTIONS, a11yViolationsLog)
    })
})
