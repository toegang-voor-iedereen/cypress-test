Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Document upload', () => {
    beforeEach(() => {
        //log in als redacteur
        cy.login(Cypress.env('username3'), Cypress.env('password3'))

        //verwijder alle documenten
        cy.deleteDocuments()
    })

    it('Een redacteur kan succesvol één .docx uploaden via het prompt.', () => {
        cy.visit('/documenten')
        cy.documentUploadSelect('docx/documentUpload_1.docx')
        cy.get('table', { timeout: 60000 }).should('contain.text', `Document Upload 1`)
    })

    it('Een redacteur kan succesvol meerdere .docx uploaden via het prompt.', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(
            [
                'cypress/fixtures/docx/documentUpload_2.docx',
                'cypress/fixtures/docx/documentUpload_3.docx',
                'cypress/fixtures/docx/documentUpload_4.docx',
            ],
            { force: true }
        )
        cy.get('#progress-container').should('contain.text', '3 documenten succesvol')
        cy.get('table', { timeout: 60000 }).should('contain.text', `Document Upload 2`)
        cy.get('table', { timeout: 60000 }).should('contain.text', `Document Upload 3`)
        cy.get('table', { timeout: 60000 }).should('contain.text', `Document Upload 4`)
    })

    it('Een redacteur kan succesvol één .docx uploaden via drag-and-drop.', () => {
        cy.visit('/documenten')
        cy.documentUploadDropzone('docx/documentUpload_5.docx')
        cy.get('table', { timeout: 60000 }).should('contain.text', `Document Upload 5`)
    })

    it('Een redacteur kan succesvol meerdere .docx uploaden via drag-and-drop.', () => {
        cy.visit('/documenten')

        cy.get('#pt-dragzone').selectFile(
            [
                'cypress/fixtures/docx/documentUpload_6.docx',
                'cypress/fixtures/docx/documentUpload_7.docx',
                'cypress/fixtures/docx/documentUpload_8.docx',
            ],
            { action: 'drag-drop' }
        )

        cy.get('#progress-container').should('contain.text', '3 documenten succesvol')

        cy.get('table', { timeout: 60000 }).should('contain.text', `Document Upload 6`)
        cy.get('table', { timeout: 60000 }).should('contain.text', `Document Upload 7`)
        cy.get('table', { timeout: 60000 }).should('contain.text', `Onderwerp`) // documentUpload_8.docx has some oddities with its title parsing
    })

    it.skip('Een redacteur kan succesvol één .pdf uploaden via het prompt.', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan succesvol één .pdf uploaden via drag-and-drop.', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan succesvol meerdere .pdf uploaden via drag-and-drop.', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it('Een redacteur sluit de browser gedurende het uploaden', () => {
        cy.visit('/documenten')
        cy.get('input[type="file"]').selectFile(`cypress/fixtures/docx/document_34mb.docx`, {
            force: true,
        })

        // The reload is our "Closed the browser"
        cy.reload()

        cy.get('table').should('not.contain.text', `document_34mb.docx`)
    })

    it('Een redacteur upload 1 document die niet valide is', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/pixel.jpg'], {
            force: true,
        })

        cy.get('#progress-container').should('contain.text', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )

        cy.get('.error-messages').should('contain.text', 'Documenttype niet ondersteund')
    })

    it('Een redacteur upload een set aan documenten waarvan er 2 niet valide zijn', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(
            [
                {
                    contents: Cypress.Buffer.from('file contents'),
                    fileName: 'file.txt',
                    mimeType: 'text/plain',
                    lastModified: Date.now(),
                },
                {
                    contents: Cypress.Buffer.from('file contents'),
                    fileName: 'file2.txt',
                    mimeType: 'text/plain',
                    lastModified: Date.now(),
                },
                'cypress/fixtures/docx/documentUpload_1.docx',
            ],
            {
                force: true,
            }
        )
        cy.get('#progress-container').should('contain.text', '1 document succesvol geüpload')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er zijn fouten opgetreden tijdens het uploaden, 2 documenten konden niet worden geüpload.'
        )

        cy.get('.error-messages').should('contain.text', 'Documenttype niet ondersteund')
    })

    it('Een redacteur upload een set aan documenten die allemaal niet valide zijn', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(
            [
                {
                    contents: Cypress.Buffer.from('file contents'),
                    fileName: 'file.txt',
                    mimeType: 'text/plain',
                    lastModified: Date.now(),
                },
                {
                    contents: Cypress.Buffer.from('file contents'),
                    fileName: 'file2.txt',
                    mimeType: 'text/plain',
                    lastModified: Date.now(),
                },
            ],
            {
                force: true,
            }
        )
        cy.get('#progress-container').should(
            'contain.text',
            'Geen documenten geüpload, 2 documenten gefaald.'
        )
        cy.get('.error-messages-summary').should(
            'contain',
            'Er zijn fouten opgetreden tijdens het uploaden, 2 documenten konden niet worden geüpload.'
        )

        cy.get('.error-messages').should('contain.text', 'Documenttype niet ondersteund')
    })

    it.skip('Een redacteur upload een docx met een script erin (hack)', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it('Een redacteur upload een jpg vermomd als docx', () => {
        cy.fixture('misc/pixel.jpg', null).then((fixture) => {
            cy.visit('/documenten')
            cy.get('input[type=file]').selectFile(
                [
                    {
                        contents: fixture,
                        fileName: 'actually-a-jpg.docx',
                        mimeType:
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        lastModified: Date.now(),
                    },
                ],
                {
                    force: true,
                }
            )
            cy.get('#progress-container').should('contain.text', '1 document succesvol geüpload')

            cy.get('table tr.document-in-error', { timeout: 60000 }).should(
                'contain.text',
                `actually-a-jpg.docx`
            )
        })
    })

    it.skip('Een redacteur upload een docx met annotaties', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur upload een docx met een SVG met script tag (hack)', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it('Een redacteur upload een .doc document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/doc-document.doc'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it('Een redacteur upload een .html document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/html-document.html'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it('Een redacteur upload een .rtf document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/rtf-document.rtf'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it('Een redacteur upload een .odt document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/odt-document.odt'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it('Een redacteur upload een .txt document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/txt-document.txt'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it('Een redacteur upload een .md document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/md-document.md'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it('Een redacteur upload een .xml document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/xml-document.xml'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it.skip('Een redacteur upload een iBabs document', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur upload een document met gemodificeerd magic number (hack)', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it('Een redacteur upload een .pages document', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/pages-document.pages'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it.skip('Een redacteur upload een .docx met 2007 Word document', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur upload 100 .docx documenten', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur upload een folder .docx documenten', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it('Een redacteur upload een .zip-file met .docx', () => {
        cy.visit('/documenten')
        cy.get('input[type=file]').selectFile(['cypress/fixtures/misc/docx-zip.zip'], {
            force: true,
        })
        cy.get('.progress--text').should('contain', '1 document gefaald')
        cy.get('.error-messages-summary').should(
            'contain',
            'Er is een fout opgetreden tijdens het uploaden, 1 document kon niet worden geüpload.'
        )
    })

    it.skip('Een redacteur upload 100 .docx documenten', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur upload een groot .docx document (file size 97mb)', () => {
        cy.visit('/documenten')
        cy.documentUploadSelectAwaitConversion('docx/document_97mb.docx', 'Groot Document')
    })

    it.skip('Een redacteur upload een docx met heel veel paginas (usability) (Pandoc performance)', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur upload een docx met heel veel fouten (usability)', () => {
        cy.visit('/documenten')
        //ToDo
    })
})
