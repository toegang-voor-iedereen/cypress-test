/* eslint-disable cypress/no-unnecessary-waiting */

import { visitPath } from 'cypress/support/helpers'

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Autorisatie', () => {
    it('Een niet-ingelogde gebruiker kan niet bij de NLdoc-omgeving', () => {
        visitPath({ path: 'documenten', failOnStatusCode: false }).then((response) => {
            expect(response.status).to.eq(403)
        })
    })

    it('Een redacteur kan succesvol inloggen en alleen documenten zien uit de eigen groep', () => {
        cy.step('Annette Organisatiebeheerder upload een document')
        cy.login(Cypress.env('username2'), Cypress.env('password2'))
        cy.visit('/documenten')
        cy.get('span.user__name').should('have.text', 'Annette Organisatiebeheerder')
        cy.step('Upload een document')
        cy.documentUploadSelectAwaitConversion(
            'docx/document-organisatiebeheerder.docx',
            'Document Organisatiebeheerder'
        )
        cy.contains('a', 'Uitloggen').click()
        cy.contains('a', 'Inloggen').should('exist')
        cy.step('Log in als Jan Redacteur')
        cy.login(Cypress.env('username3'), Cypress.env('password3'))
        cy.visit('/documenten')
        cy.get('span.user__name').should('have.text', 'Jan Redacteur')
        cy.step('Controleer of document van Organisatiebeheerder zichtbaar is voor redacteur')
        cy.contains('.views-table', 'Document Organisatiebeheerder')
        cy.step('controleer of alleen documenten van de leden van dezelfde groep zichtbaar zijn')
        cy.get('table tbody tr td:second-child').then((cells) => {
            const names = [...cells].map((cell) => cell.innerText.trim())

            const expectedNames = ['Annette Organisatiebeheerder', 'Piet Admin', 'Jan Redacteur']

            expect(names).to.have.members(expectedNames)
        })
    })

    it('Een gebruiker kan maar lid zijn van één groep', () => {
        cy.login(Cypress.env('username1'), Cypress.env('password1')) //Log in als admin
        cy.visit('/documenten')
        cy.get('span.user__name').should('have.text', 'Piet Admin')
        cy.step(
            'Jan redacteur is lid van Gemeente Testland. Voeg hem toe aan Waterschap Testersveld'
        )
        cy.visit('/admin/group')
        cy.get('h1.page-title').should('have.text', 'Groepen')
        cy.contains('a', 'Waterschap Testersveld').click()
        cy.step('Voeg Jan redacteur toe aan groep')
        cy.contains('a', 'Members').click({ force: true })
        cy.contains('a', 'Lid toevoegen').click()
        cy.get('h1.page-title').should('have.text', 'Groepslidmaatschap toevoegen')
        cy.get('#edit-entity-id-0-target-id').type('JanRedacteur').type('{enter}')
        cy.get('.form-item__error-message').should('contain', 'iets van een foutmelding')
    })

    it.skip('Een gebruiker kan succesvol verwijderd worden uit een groep', () => {
        // TODO : add check op succevol kunnen verwijderen en geen documenten meer kunnen zien
    })
})
