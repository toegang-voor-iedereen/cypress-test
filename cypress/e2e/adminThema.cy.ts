/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Visuele tests op het adminthema', () => {
    beforeEach(() => {
        cy.eyesOpen({
            // The name of the app under test
            appName: 'NLdoc adminthema',
            // The name of the test case
            testName: Cypress.currentTest.title,
        })
    })

    afterEach(() => {
        cy.eyesClose()
    })

    it.skip('De landingspagina voldoet aan het ontwerp', () => {
        cy.visit('/')

        cy.get('[class="dialog-off-canvas-main-canvas"]').should('be.visible')

        cy.eyesCheckWindow()
    })

    it.skip('De loginpagina voldoet aan het ontwerp', () => {
        cy.visit('/user/login')

        cy.eyesCheckWindow()
    })

    it.skip('Het documentenoverzicht voldoet aan het ontwerp', () => {
        cy.login(Cypress.env('username3'), Cypress.env('password3'))

        cy.visit('/documenten')
        cy.eyesCheckWindow({
            ignore: [
                { selector: '.view-content' },
                { selector: '.view-footer' },
                { selector: '.page' },
            ],
        })
    })
})
