/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Redirection test van HTTPS en WWW', () => {
    it('URL redirect van HTTP WWW naar HTTPS WWW', () => {
        cy.request({
            url: 'http://www.nldoc.nl',
            followRedirect: false,
        }).then((response) => {
            expect(response.status).to.eq(302)

            const redirectLocation = response.headers['location']
            cy.log(`${redirectLocation}`)
            expect(redirectLocation.includes('https')).to.be.true
            expect(redirectLocation.includes('www')).to.be.true
        })
    })

    it('URL redirect van HTTP naar HTTPS', () => {
        cy.request({
            url: 'http://nldoc.nl',
            followRedirect: false,
        }).then((response) => {
            expect(response.status).to.eq(302)

            const redirectLocation = response.headers['location']
            cy.log(`${redirectLocation}`)
            expect(redirectLocation.includes('https')).to.be.true
            expect(redirectLocation.includes('www')).to.be.false
        })
    })

    it('URL redirect van WWW naar non-www', () => {
        cy.request({
            url: 'http://www.nldoc.nl',
            followRedirect: false,
        }).then((response) => {
            expect(response.status).to.eq(301)

            const redirectLocation = response.headers['location']
            cy.log(`${redirectLocation}`)
            expect(redirectLocation.includes('https')).to.be.true
            expect(redirectLocation.includes('www')).to.be.false
        })
    })
})
