Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Document bewerken', () => {
    beforeEach(() => {
        //log in als redacteur
        cy.login(Cypress.env('username3'), Cypress.env('password3'))
        cy.deleteDocuments()
    })

    it('Een redacteur kan een document bewerken.', () => {
        cy.visit('/documenten')
        cy.documentUploadSelectAwaitConversion(
            'docx/documentBewerken.docx',
            'Document Bewerken Test'
        )
        cy.editDocument('Document Bewerken Test')

        cy.step('Een redacteur kan de title tekstueel aanpassen')
        cy.get('#skip-to-title').should('have.text', 'Titel (h1) bewerken')
        cy.get('#field_title_long').clear()
        cy.get('#field_title_long').type('Document Bewerken Test is gewijzigd')

        cy.get('#edit-concept').click()
        cy.url().should('include', '/assistant')

        cy.get('h1 > .field').contains('Document Bewerken Test is gewijzigd')

        cy.step('Een redacteur kan een H1 tekstueel aanpassen')

        cy.contains('Heading 1').parent('div').parent('div').find('a').click()
        //ToDo
        cy.step('Een redacteur kan een H2 tekstueel aanpassen')
        cy.step('Een redacteur kan een H3 tekstueel aanpassen')
        cy.step('Een redacteur kan een H4 tekstueel aanpassen')
        cy.step('Een redacteur kan een H5 tekstueel aanpassen')
        cy.step('Een redacteur kan een H6 tekstueel aanpassen')
        cy.step('Een redacteur kan een P tekstueel aanpassen')
    })

    it.skip('Een redacteur kan aangeven dat een afbeelding (img) decoratief is', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan aangeven dat een afbeelding (img) niet-decoratief is', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan een UL tekstueel aanpassen', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan een OL tekstueel aanpassen', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan een interne link toevoegen', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan een externe link toevoegen', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Een redacteur kan een externe link toevoegen', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('De inhoud van de metadata is correct', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Het wijzigen van het publicatietype is succesvol', () => {
        cy.visit('/documenten')
        //ToDo
    })

    it.skip('Het wijzigen van het onderwerp is succesvol', () => {
        cy.visit('/documenten')
        //ToDo
    })
})
