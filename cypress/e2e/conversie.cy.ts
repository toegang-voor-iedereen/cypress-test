/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err)
    return false
})

describe('Conversie', () => {
    beforeEach(() => {
        //log in als redacteur
        cy.login(Cypress.env('username3'), Cypress.env('password3'))
        cy.deleteDocuments()
    })

    it.only('Het happy flow document kan succesvol geconverteerd worden', () => {
        cy.visit('/documenten')

        cy.documentUploadSelectAwaitConversion('docx/happy-flow.docx', 'Een happy flow document')
        cy.editDocument('Een happy flow document')

        cy.step('The document should be accessible')
        cy.get('.accessibility__errors').should('contain.text', 'Dit document is toegankelijk')

        cy.step('Publish the document')
        cy.get('input[value="Publiceren"]').scrollIntoView()
        cy.get('input[value="Publiceren"]').click()

        cy.get('.nldoc-html>*:nth-child(2)')
            .should('contain.text', 'Een happy flow document')
            .should('match', 'h1')
        cy.get('.nldoc-html>*:nth-child(3)')
            .should('contain.text', 'Dit is een H1')
            .should('match', 'h2')
        cy.get('.nldoc-html>*:nth-child(4)')
            .should('contain.text', 'Dit is een paragraaf. Lorem ipsum dolor sit amet,')
            .should('match', 'p')
        cy.get('.nldoc-html>*:nth-child(5)')
            .should('contain.text', 'Morbi porta nec tortor')
            .should('match', 'p')
        cy.get('.nldoc-html>*:nth-child(6)')
            .should('contain.text', 'Dit is een h2')
            .should('match', 'h3')
        cy.get('.nldoc-html>*:nth-child(7)')
            .should('contain.text', 'Dit is een paragraaf')
            .should('match', 'p')
        cy.get('.nldoc-html>*:nth-child(8)')
            .should('contain.text', 'Een ongeordende lijst')
            .should('match', 'ul')
        cy.get('.nldoc-html>*:nth-child(9)')
            .should('contain.text', 'Dit is een h3')
            .should('match', 'h4')
        cy.get('.nldoc-html>*:nth-child(10)')
            .should('contain.text', 'Dit is een paragraaf')
            .should('match', 'p')
        cy.get('.nldoc-html>*:nth-child(11)')
            .should('contain.text', 'Dit is een genummerde lijst')
            .should('match', 'ol')
        cy.get('.nldoc-html>*:nth-child(12)')
            .should('contain.text', 'Dit is een h2')
            .should('match', 'h3')
        cy.get('.nldoc-html>*:nth-child(13)')
            .should('contain.text', 'Morbi porta nec tortor')
            .should('match', 'p')
        cy.get('.nldoc-html>*:nth-child(14)')
            .should('contain.text', 'Is dit beleid')
            .should('match', 'p')
        cy.get('.nldoc-html>*:nth-child(15)')
            .should('contain.text', 'Dit is een h2')
            .should('match', 'h3')
        cy.get('.nldoc-html>*:nth-child(16)')
            .should('contain.text', 'Morbi porta nec tortor')
            .should('match', 'p')
        cy.get('.nldoc-html>*:nth-child(17)')
            .should('contain.text', 'Tabel header 1')
            .should('match', 'table')
        cy.step('controleer documentinformatie na publicatie')
        cy.get('#nldoc-metadata-button').should('be.visible').click()

        cy.get('h2#metadata-panel-heading').should('be.visible')
        cy.get('h2#metadata-panel-heading').should('have.text', 'Informatie over dit document')

        cy.get('dt.utrecht-data-list__item-key').eq(0).should('have.text', 'Titel')
        cy.get('dd.utrecht-data-list__item-value').eq(0).should('not.be.empty')

        cy.get('dt.utrecht-data-list__item-key')
            .eq(1)
            .should('have.text', 'Publicerende organisatie')
        cy.get('dd.utrecht-data-list__item-value').eq(1).should('not.be.empty')

        cy.get('dt.utrecht-data-list__item-key').eq(2).should('have.text', 'Publicatie datum')
        //Check if date is in the correct format
        cy.get('dd.utrecht-data-list__item-value')
            .eq(2)
            .invoke('text')
            .then((text) => {
                const datePattern = /\b\d{2}-\d{2}-\d{4}\b/
                expect(text).to.match(datePattern)
            })
    })

    it('na het introduceren van een validatiemelding kan deze opgelost worden', () => {
        cy.visit('/documenten')

        cy.documentUploadSelectAwaitConversion('docx/happy-flow.docx', 'Een happy flow document')
        cy.editDocument('Een happy flow document')

        cy.step('The document should be accessible')
        cy.get('.accessibility__errors').should('contain.text', 'Dit document is toegankelijk')

        cy.step('Change the first h2 to an h3')
        cy.get('.frontend-editing.paragraph').first().should('contain.text', 'Dit is een H1')
        cy.get('.frontend-editing.paragraph a.frontend-editing__action').first().click()
        cy.getIframeBody()
        cy.wait(500)
        cy.getIframeBody().find('#edit-field-heading-0-container-size').select('h3')
        cy.getIframeBody().find('#edit-submit').click()

        cy.step('The document should not be accessible')
        cy.get('.accessibility__errors').scrollIntoView()
        cy.get('.accessibility__errors').should('contain.text', 'Dit document is niet toegankelijk')

        cy.step('The publish button should be disabled')
        cy.get('input[value="Publiceren"]').scrollIntoView()
        cy.get('input[value="Publiceren"]').should('be.disabled')

        cy.step('Change the heading back to an h2')
        cy.get('.frontend-editing.paragraph').first().scrollIntoView()
        cy.get('.frontend-editing.paragraph').first().should('contain.text', 'Dit is een H1')
        cy.get('.frontend-editing.paragraph a.frontend-editing__action').first().click()
        cy.getIframeBody().find('#edit-field-heading-0-container-size').select('h2')
        cy.getIframeBody().find('#edit-submit').click()

        cy.step('The document should be accessible')
        cy.get('.accessibility__errors').scrollIntoView()
        cy.get('.accessibility__errors').should('contain.text', 'Dit document is toegankelijk')

        cy.step('The publish button should not be disabled')
        cy.get('input[value="Publiceren"]').scrollIntoView()
        cy.get('input[value="Publiceren"]').should('not.be.disabled')

        cy.step('Publish the document')
        cy.get('input[value="Publiceren"]').click()
        cy.get('h1.page-title').should('contain.text', 'Een happy flow document')
    })
})
